package by.step.webproductlist.repository

import by.step.retrofit.api.ProductsApiService
import by.step.webproductlist.dto.ProductsMapper
import javax.inject.Inject

class ProductsRepository @Inject constructor(
    private val apiService: ProductsApiService
) {

    suspend fun getProductList() = apiService.getProductList()

    suspend fun deleteProduct(pid: String) =
        apiService.deleteProduct(ProductsMapper.mapProductDeleteRequest(pid))
}