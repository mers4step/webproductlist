package by.step.webproductlist.dto;

public enum Status {
    SUCCESS,
    ERROR,
    LOADING
}
