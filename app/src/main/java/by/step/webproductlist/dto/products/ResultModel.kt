package by.step.retrofit.domain.dto.products

class ResultModel(

    val success: Int,
    val message: String
)