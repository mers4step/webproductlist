package by.step.webproductlist.dto

import by.step.retrofit.domain.dto.products.ProductModel
import by.step.retrofit.domain.dto.products.ResultModel
import by.step.webproductlist.api.request.ProductDeleteRequest
import by.step.webproductlist.api.response.ProductDeleteResponse
import by.step.webproductlist.api.response.ProductListResponse
import by.step.webproductlist.dto.products.ProductListModel

object ProductsMapper {

    fun map(response: ProductListResponse) =
        ProductListModel(
//            productList = response.products.sortedBy { it.pid.toInt() }.map { mapProductModel(it) },
            productList = response.products.map { mapProductModel(it) },
            isSuccess = response.success == 1,
            message = response.message
        )

    private fun mapProductModel(product: ProductListResponse.Product) =
        ProductModel(
            pid = product.pid,
            name = product.name,
            price = product.price,
            created = product.created,
            updated = product.updated
        )

    fun map(response: ProductDeleteResponse) =
        ResultModel(
            success = response.success,
            message = response.message
        )

    fun mapProductDeleteRequest(pid: String) =
        ProductDeleteRequest(pid)
}