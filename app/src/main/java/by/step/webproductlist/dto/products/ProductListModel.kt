package by.step.webproductlist.dto.products

import by.step.retrofit.domain.dto.products.ProductModel

class ProductListModel(

    val productList: List<ProductModel>,
    val isSuccess: Boolean,
    val message: String?
)