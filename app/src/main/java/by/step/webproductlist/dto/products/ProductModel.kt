package by.step.retrofit.domain.dto.products

class ProductModel(

    val pid: String,
    val name: String,
    val price: String,
    val created: String,
    val updated: String
)