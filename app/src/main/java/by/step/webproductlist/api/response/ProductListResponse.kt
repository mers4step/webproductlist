package by.step.webproductlist.api.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class ProductListResponse(

    @Expose
    @SerializedName("products")
    val products: List<Product>,

    @Expose
    @SerializedName("success")
    val success: Int,

    @Expose
    @SerializedName("message")
    val message: String?
) {

    class Product(

        @Expose
        @SerializedName("pid")
        val pid: String,

        @Expose
        @SerializedName("name")
        val name: String,

        @Expose
        @SerializedName("price")
        val price: String,

        @Expose
        @SerializedName("created_at")
        val created: String,

        @Expose
        @SerializedName("updated_at")
        val updated: String
    )
}
