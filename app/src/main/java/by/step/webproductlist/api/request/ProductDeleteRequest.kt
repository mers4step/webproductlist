package by.step.webproductlist.api.request

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class ProductDeleteRequest(

    @Expose
    @SerializedName("pid")
    val pid: String
)