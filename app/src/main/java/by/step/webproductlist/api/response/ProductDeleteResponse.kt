package by.step.webproductlist.api.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class ProductDeleteResponse(

    @Expose
    @SerializedName("success")
    val success: Int,

    @Expose
    @SerializedName("message")
    val message: String
)