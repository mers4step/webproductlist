package by.step.retrofit.api

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object ProductsApiModule {

    @Provides
    @Singleton
    fun provideApiService(retrofit: Retrofit): ProductsApiService =
        retrofit.create(ProductsApiService::class.java)
}