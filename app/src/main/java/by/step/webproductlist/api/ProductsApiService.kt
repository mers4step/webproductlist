package by.step.retrofit.api

import by.step.webproductlist.api.request.ProductDeleteRequest
import by.step.webproductlist.api.response.ProductDeleteResponse
import by.step.webproductlist.api.response.ProductListResponse
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

interface ProductsApiService {

    @GET("prod4step/get_all_products.php")
    suspend fun getProductList(): Response<ProductListResponse>

    @POST("prod4step/delete_product_json.php")
    suspend fun deleteProduct(@Body request: ProductDeleteRequest): Response<ProductDeleteResponse>
}