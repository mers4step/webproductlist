package by.step.webproductlist.ui.fragmentview

interface IActionView {

    fun showSnackBar(message: String)

    fun showProgressView()

    fun hideProgressView()
}
