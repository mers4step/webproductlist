package by.step.webproductlist.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import by.step.retrofit.domain.dto.Resource
import by.step.retrofit.domain.dto.products.ResultModel
import by.step.webproductlist.dto.ProductsMapper
import by.step.webproductlist.dto.products.ProductListModel
import by.step.webproductlist.repository.ProductsRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ProductsViewModel @Inject constructor(
    private val repository: ProductsRepository,
) : ViewModel() {

    var productListLivData = MutableLiveData<Resource<ProductListModel>>()
    var deleteResultLivData = MutableLiveData<Resource<ResultModel>>()

    fun getProductList() =
        viewModelScope.launch(Dispatchers.IO + CoroutineExceptionHandler { _, throwable ->
            throwable.printStackTrace()
            productListLivData.postValue(Resource.error(throwable.localizedMessage, null))
        }) {
            repository.getProductList().let {
                if (it.isSuccessful) it.body()?.let { response ->
                    productListLivData.postValue(
                        Resource.success(
                            ProductsMapper.map(response)
                        )
                    )
                }
                else productListLivData.postValue(Resource.error(it.errorBody().toString(), null))
            }
        }

    fun deleteProduct(pid: String) =
        viewModelScope.launch(Dispatchers.IO + CoroutineExceptionHandler { _, throwable ->
            throwable.printStackTrace()
            deleteResultLivData.postValue(Resource.error(throwable.localizedMessage, null))
        }) {
            repository.deleteProduct(pid).let {
                if (it.isSuccessful) it.body()?.let { response ->
                    deleteResultLivData.postValue(
                        Resource.success(
                            ProductsMapper.map(response)
                        )
                    )
                }
                else deleteResultLivData.postValue(Resource.error(it.errorBody().toString(), null))
            }
        }
}