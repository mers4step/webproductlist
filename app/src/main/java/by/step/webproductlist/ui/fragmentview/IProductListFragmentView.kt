package by.step.webproductlist.ui.fragmentview

import by.step.retrofit.domain.dto.products.ProductModel
import by.step.webproductlist.dto.products.ProductListModel

interface IProductListFragmentView : IActionView {

    fun populate(model: ProductListModel)

    fun setListener(listener: Listener)

    interface Listener {

        fun onItemLongClick(model: ProductModel)

        fun onListRefresh()
    }
}