package by.step.webproductlist.ui.fragment

import android.app.AlertDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import by.step.retrofit.domain.dto.Resource
import by.step.retrofit.domain.dto.products.ProductModel
import by.step.retrofit.domain.dto.products.ResultModel
import by.step.webproductlist.R
import by.step.webproductlist.dto.Status
import by.step.webproductlist.dto.products.ProductListModel
import by.step.webproductlist.ui.fragmentview.IProductListFragmentView
import by.step.webproductlist.ui.viewmodel.ProductsViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ProductListFragment : Fragment() {

    private val viewModel: ProductsViewModel by viewModels()
    private lateinit var view: IProductListFragmentView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val rootView = inflater.inflate(R.layout.product_list_fragment_view, container, false)
        view = rootView as IProductListFragmentView
        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        this.view.setListener(object : IProductListFragmentView.Listener {

            override fun onItemLongClick(model: ProductModel) {
                showDeleteDialog(model)
            }

            override fun onListRefresh() {
                viewModel.getProductList()
            }
        })
        subscribe()
        viewModel.getProductList()
    }

    private fun subscribe() {
        viewModel.productListLivData.observe(viewLifecycleOwner, { processChanged(it) })
        viewModel.deleteResultLivData.observe(viewLifecycleOwner, { processChangedDelete(it) })
    }

    private fun processChanged(model: Resource<ProductListModel>) {
        when (model.status) {
            Status.SUCCESS -> model.data?.let {
                view.hideProgressView()
                view.populate(it)
            }
            Status.LOADING -> view.showProgressView()
            Status.ERROR -> {
                model.message?.let { view.showSnackBar(it) }
                view.hideProgressView()
            }
        }
    }

    private fun processChangedDelete(model: Resource<ResultModel>) {
        when (model.status) {
            Status.SUCCESS -> {
                view.hideProgressView()
                viewModel.getProductList()
            }
            Status.LOADING -> view.showProgressView()
            Status.ERROR -> {
                model.message?.let { view.showSnackBar(it) }
                view.hideProgressView()
            }
        }
    }


    private fun showDeleteDialog(model: ProductModel) {
        AlertDialog.Builder(context).setTitle("Удалить " + model.name + "?")
            .setPositiveButton(
                "Да"
            ) { _, _ -> viewModel.deleteProduct(model.pid) }
            .show()
    }

    companion object {

        fun newInstance() = ProductListFragment()
    }
}