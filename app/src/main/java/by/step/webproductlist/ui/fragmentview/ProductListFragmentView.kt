package by.step.webproductlist.ui.fragmentview

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.FrameLayout
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import by.step.retrofit.domain.dto.products.ProductModel
import by.step.webproductlist.R
import by.step.webproductlist.dto.products.ProductListModel
import by.step.webproductlist.ui.adaprer.ProductsAdapter
import by.step.webproductlist.ui.bind
import com.google.android.material.snackbar.Snackbar

class ProductListFragmentView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr), IProductListFragmentView {

    private val swipeRefreshLayout: SwipeRefreshLayout by bind(R.id.swipe_refresh)
    private val productListView: RecyclerView by bind(R.id.products_list_view)
    private val progressView: FrameLayout by bind(R.id.progress_view)

    private var listener: IProductListFragmentView.Listener? = null

    override fun populate(model: ProductListModel) {
        swipeRefreshLayout.setOnRefreshListener {
            listener?.onListRefresh()
            swipeRefreshLayout.isRefreshing = false
        }
        val adapter = ProductsAdapter(model.productList)
        productListView.layoutManager = LinearLayoutManager(this.context)
        productListView.adapter = adapter
        adapter.setListener(object : ProductsAdapter.Listener {

            override fun onItemLongClick(model: ProductModel) {
                listener?.onItemLongClick(model)
            }
        })
    }

    override fun showProgressView() {
        progressView.visibility = View.VISIBLE
        productListView.visibility = View.GONE
    }

    override fun hideProgressView() {
        progressView.visibility = View.GONE
        productListView.visibility = View.VISIBLE
    }

    override fun setListener(listener: IProductListFragmentView.Listener) {
        this.listener = listener
    }

    override fun showSnackBar(message: String) {
        Snackbar.make(this, message, Snackbar.LENGTH_LONG).show()
    }
}