package by.step.webproductlist.ui.adaprer

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import by.step.retrofit.domain.dto.products.ProductModel
import by.step.webproductlist.R

class ProductsAdapter(private val list: List<ProductModel>) :
    RecyclerView.Adapter<ProductsAdapter.ProductViewHolder>() {

    private var listener: Listener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.product_list_item, parent, false)
        return ProductViewHolder(view)
    }

    override fun onBindViewHolder(holder: ProductViewHolder, position: Int) {
        holder.populate(list[position])
        holder.itemView.setOnLongClickListener {
            listener?.onItemLongClick(list[position])
            true
        }
    }

    override fun getItemCount() = list.size

    inner class ProductViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val idTextView = itemView.findViewById<TextView>(R.id.product_id)
        private val nameTextView = itemView.findViewById<TextView>(R.id.product_name)

        fun populate(model: ProductModel) {
            idTextView.text = model.pid
            nameTextView.text = model.name
        }
    }

    fun setListener(listener: Listener) {
        this.listener = listener
    }

    interface Listener {

        fun onItemLongClick(model: ProductModel)
    }
}