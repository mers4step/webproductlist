package by.step.webproductlist.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import by.step.webproductlist.R
import by.step.webproductlist.ui.fragment.ProductListFragment
import dagger.hilt.android.AndroidEntryPoint

//https://developer.android.com/training/dependency-injection/hilt-android
@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity)
        supportFragmentManager.beginTransaction()
                .replace(R.id.container, ProductListFragment.newInstance())
                .commitNow()
    }
}